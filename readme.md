# Description
Here are the weather icons I had previously constructed for use with the Yahoo Weather API with Adobe Flash and later for progressively enhanced html5 consumption.

## Some Historic Background
I had previously constructed these icons in Adobe Fireworks. The latter, by the way, in my opinion should open&#45;sourced if it is not going to return to market anytime soon. I do not have access to the latter either anymore and in my only opinion I firmly believe that nobody should be denied access to these icons if they desired to go with a quick hack until they can roll their own&hellip; or decide to stick with mine. A long story made short &#45; I would be delighted if you were to roll mine, too. 

## The Weather Icons
Presently, there will not be a site associated with this micro project. I have kept the original .png formatted documents in 600px&#45;squared size, plus, had rendered a set of 256px&#45;squared size images in .gif format. Both, obviously have their alpha channels, hence, the built&#45;in transparencies.

## But why any icons at all?
I had constructed these icons originally because I had built a weather app for Adobe Flash. Then, around the time progressive enhancement became a thing I would also write the html version that would be reconstituted as php. All of it communicated with the [Yahoo Weather API](https://developer.yahoo.com/weather/ "Yahoo Weather API"). These days, however, I would prefer the [Open Weather API](http://openweathermap.org/api "Open Weather API") any day of the week! And, I would opt for front end AJAX with javascript, in lieu of, the loathsome php AJAX version I had going previously. EEEESH! One day, Lord willing, I will put the old php version and the yet to be built javascript version. Then all of you can decide for yourselves that is if tomorrow ever comes. 

### Example context for future use
The first place I would use it would would be in a weather app called Breeze that was beautifully constructed several years ago by a gentleman named Ivan whom I have never met, btw. Uh, hello Ivan! The details are contained within. I had originally seen the republished version at the now defunct tech [dot] pro website not too long ago before all of a sudden they flew by night. But, in their own words, [Breeze is a basic web&#45;app](https://github.com/munkychop/breeze "Breeze is a basic web&#45;app") that loads and displays the current temperature for practically any place in the world. The author also managed responsibly any use of bloated software with global objects galore by culling any possibility of it with another beautifully constructed library called Atomic. In their words, [Atomic](https://github.com/cferdinandi/atomic "Atomic")-- originally by Todd Motto --is a handler for [&hellip;] &quot;vanilla JavaScript Ajax requests.&quot; It was as if the the whole exercise had fallen from the sky in a post attachEvent world. In other words, IE8 was no more in officialdom anyways. I could not have been more pleased at the time how easy it was to relearn the ubiquitous weather app.

### Additional context for possible use
The second instance I can think of is with Sam Dutton&rsquo;s prezzo on [Progressive Web Apps Training](https://developers.google.com/web/ilt/pwa/ "Progressive Web Apps Training") where, indeed, he does include some basic sunny icons but in the end, for my purposes, is essentially limited in scope since it is only a demonstration. Mine could expand on that were anyone to decide to download themselves a copy of the weather icons today. 

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!
